/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "html/*.js",
    "html/*.html"
  ],
  theme: {
    extend: {
      fontFamily: {
        'outfit': ['Outfit', 'sans-serif'],
        'rubik': ['Rubik Vinyl', 'Outfit', 'sans-serif']
      },
    },
  },
  plugins: [],
}
