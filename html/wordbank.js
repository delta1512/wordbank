const wordbankCsvUrl = 'https://d2grpx23keuulm.cloudfront.net'
const PAGE_SIZE = 20
var currentPage = 1
var wordbank


function appendWordComponent(word) {
  wordComponent = document.createElement('div')
  wordComponent.className = "flex flex-row items-center justify-center"
  wordComponent.innerHTML = `
  <img class="m-7 w-48 h-48 basis-1/3 flex-col" src="${wordbankCsvUrl}/images/${word}" alt="${word}">
  <p class="m-7 basis-1/3 text-5xl flex-col text-center">${word}</p>
  <img class="m-7 basis-1/3 flex-col" onclick="document.getElementById('${word}-audio').play()" src="res/play.svg" width="100px" height="100px">

  <audio id="${word}-audio"style="display:none;">
    <source src="${wordbankCsvUrl}/audio/${word}.mp3" type="audio/mpeg">
  </audio>
  `
  document.getElementById('wordbank').appendChild(wordComponent)
}


function loadPage(i) {
  let idx = i - 1
  wordbank.slice(idx * PAGE_SIZE, i * PAGE_SIZE).forEach(w => appendWordComponent(w))
}


function loadWordbank() {
  fetch(wordbankCsvUrl)
  .then(resp => resp.text())
  .then(resp => wordbank = resp.split('\n').sort().slice(1))
  .then(resp => loadPage(currentPage))
  .then(resp => document.getElementById('loading').style.display = 'none')
}

function nextPage() {
  if (currentPage < Math.ceil(wordbank.length / PAGE_SIZE)) {
    document.getElementById('wordbank').innerHTML = ''
    currentPage++
    loadPage(currentPage)
  }
}

function prevPage() {
  if (currentPage > 1) {
    document.getElementById('wordbank').innerHTML = ''
    currentPage--
    loadPage(currentPage)
  }
}