import os
import csv
import sys
import time
from io import BytesIO

from duckduckgo_search import ddg_images
from PIL import Image
import requests
import boto3


AWS_REGION = 'ap-southeast-2'


def fetch_audio(word: str, client=None):
    if client is None:
        client = boto3.client('polly', region_name=AWS_REGION)

    speech = client.synthesize_speech(
        Engine='neural',
        TextType='ssml',
        VoiceId='Olivia',
        OutputFormat='mp3',
        Text=f'<speak><prosody rate="50%">{word}</prosody></speak>'
    )

    with open(f'wordbank/audio/{word}.mp3', 'wb') as af:
        af.write(speech['AudioStream'].read())

    return True


def fetch_image(word: str):
    results = ddg_images(
        word,
        max_results=10,
        layout='square',
        license_image='any',
        type_image='clipart'
    )

    # in the rare case that nothing is found, broaden the search
    if len(results) == 0:
        time.sleep(3) # Be courteous to the DDG servers
        results = ddg_images(word, max_results=100, license_image='any')

    imgs = filter(lambda i: i['image'][-4:].lower() in ('.png', '.jpg', 'jpeg'), results)

    # Keep going through the images until one of them works
    for img in imgs:
        img_url = img['image']

        try:
            r = requests.get(img_url)
        except requests.RequestException:
            continue

        if r.status_code == 200:
            try:
                with Image.open(BytesIO(r.content)) as im:
                    im.thumbnail((300, 300))
                    im.save(f'wordbank/images/{word}', 'WEBP', quality=80)
            except:
                continue

            return True

    return False


def build_wordbank(input_file: str):
    client = boto3.client('polly', region_name=AWS_REGION)

    if os.path.exists('wordbank'):
        print('Warning: wordbank folder already detected. It is recommended to remove it after use.')
    else:
        os.makedirs('wordbank/images')
        os.makedirs('wordbank/audio')

    with open(input_file, 'r') as csvf:
        r = csv.reader(csvf)

        for row in r:
            word = row[0]

            if not fetch_image(word.lower()):
                print(f'Warning: could not find image for {word}')

            fetch_audio(word, client)

            time.sleep(1)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('No wordlist provided')
        sys.exit(1)

    print('Starting wordbank scraper...')
    build_wordbank(sys.argv[1])

    print('Done')
